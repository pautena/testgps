package gps.library.duffman.mycustomviews.CustomViews;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;

import gps.library.duffman.mycustomviews.R;

/**
 * Created by pau on 04/07/15.
 */
public class SearchToolbar extends Toolbar {

    public SearchToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.custom_search_action_bar, this);
    }
}
