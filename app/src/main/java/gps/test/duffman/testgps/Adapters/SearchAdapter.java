package gps.test.duffman.testgps.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import duffman.libarary.gps.DTOPlaceAutocomplete;
import gps.test.duffman.testgps.R;

/**
 * Created by pau on 06/07/15.
 */
public class SearchAdapter extends CustomAdapter<DTOPlaceAutocomplete,SearchAdapter.ViewHolder>{
    public SearchAdapter(List<DTOPlaceAutocomplete> data) {
        super(R.layout.abc_action_bar_view_list_nav_layout, data);
    }

    @Override
    public ViewHolder getInstance(View view) {
        ViewHolder viewHolder= new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        holder.mTextView.setText(getItem(i).description);
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(View v) {
            super(v);
        }
    }

}
