package gps.test.duffman.testgps;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.quinny898.library.persistentsearch.SearchBox;

import butterknife.ButterKnife;
import duffman.libarary.gps.LocationService;
import gps.test.duffman.testgps.Fragments.MainFragment;
import gps.test.duffman.testgps.Fragments.SearchFragment;


public class MainActivity extends AppCompatActivity implements SearchBox.MenuListener,SearchFragment.SearchFragmentCallback {
    private Drawer drawer;

    private SearchBox search;
    private boolean searchIsOpened=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        drawer= new DrawerBuilder().withActivity(this).build();



        setSearchFragment();
        setFragment(MainFragment.getInstance(), false);
    }

    public void setFragment(Fragment fragment,boolean addToBackStack) {
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        if(addToBackStack)

            try {
                ft.addToBackStack(null);
            }catch (IllegalArgumentException e){}


        ft.replace(R.id.container,fragment);
        ft.commit();
    }

    public void setSearchFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_search, SearchFragment.getInstance());
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMenuClick() {
        if(drawer.isDrawerOpen())
            drawer.closeDrawer();
        else
            drawer.openDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(LocationService.hasInicialized())
            LocationService.getLocationService().connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(LocationService.hasInicialized())
            LocationService.getLocationService().disconect();
    }

    @Override
    public SearchBox.MenuListener getMenuListener() {
        return this;
    }

    @Override
    public void sendSearch(String query) {
    }

    @Override
    public void setSearchBox(SearchBox search) {
        this.search=search;
    }

    @Override
    public void onSearchOpened() {
        searchIsOpened=true;
    }

    @Override
    public void onSearchClosed() {
        searchIsOpened=false;
    }

    @Override
    public void onSearchCleared() {
        searchIsOpened=true;
    }

    @Override
    public void onBackPressed() {
        if(searchIsOpened) {
            search.clearResults();
            search.toggleSearch();
        }else
            super.onBackPressed();
    }
}
