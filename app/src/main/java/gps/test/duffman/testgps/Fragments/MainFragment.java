package gps.test.duffman.testgps.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

import butterknife.Bind;
import butterknife.ButterKnife;
import duffman.libarary.gps.LocationService;
import gps.test.duffman.testgps.R;

/**
 * Created by pau on 03/07/15.
 */
public class MainFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener {
    private final static String TAG="MainFragment";

    @Bind(R.id.map_view) MapView mapView;

    private LocationService map;

    public static Fragment getInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);


        mapView.onCreate(savedInstanceState);

        LocationService.initialize(mapView.getMap(), getActivity().getApplicationContext());
        LocationService.addCallbackAndListener(this,this,this);
        LocationService.getLocationService().connect();
        map=LocationService.getLocationService();

        boolean result=map.setCameraInYourPosition();
        if(!result)
            Toast.makeText(getActivity().getApplicationContext(),"No last position",Toast.LENGTH_SHORT).show();

        return view;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConected");

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG,"lat: "+latLng.latitude+"; lng: "+latLng.longitude);
    }
}
