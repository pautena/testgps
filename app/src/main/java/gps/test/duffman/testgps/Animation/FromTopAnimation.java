package gps.test.duffman.testgps.Animation;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import gps.test.duffman.testgps.R;

/**
 * Created by pau on 05/04/15.
 */
public abstract class FromTopAnimation {

    public static void setAnimation(Context context, FrameLayout layout){
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.from_top);
        layout.startAnimation(myFadeInAnimation);
    }
}
