package duffman.libarary.gps;

import java.util.ArrayList;

/**
 * Created by pau on 05/07/15.
 */
public interface GetAutocompleteCallback {
    void onGetAutocompleteFinish(ArrayList<DTOPlaceAutocomplete> dtoPlaceAutocompletes);
}
