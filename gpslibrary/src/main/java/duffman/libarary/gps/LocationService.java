package duffman.libarary.gps;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import duffman.libarary.gps.Enums.MapZoom;

/**
 * Created by pau on 04/07/15.
 */
public class LocationService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener {
    private final static String STD_ERROR_MSG="LocationService no icinailizado";

    private static LocationService locationService=null;
    
    public static boolean hasInicialized(){ return locationService!=null;}

    public static void initialize(GoogleMap map, Context context ){
        locationService= new LocationService(map,context);
    }
    public static LocationService getLocationService(){
        if(locationService==null) throw new RuntimeException(STD_ERROR_MSG);
        else return locationService;
    }

    public static boolean addCallbackAndListener(GoogleApiClient.ConnectionCallbacks callback,
                                                 GoogleApiClient.OnConnectionFailedListener failedListener,
                                                 GoogleMap.OnMapClickListener clickListener){
        if(locationService==null) throw new RuntimeException(STD_ERROR_MSG);

        if(locationService.connectionCallbacks.contains(callback) ||
                locationService.failedListeners.contains(failedListener) ||
                locationService.onClickListeners.contains(clickListener))
            return false;

        addConectionCallback(callback);
        addFailedListener(failedListener);
        addOnClickListener(clickListener);

        return true;
    }

    public static boolean addConectionCallback(GoogleApiClient.ConnectionCallbacks callback){
        if(locationService==null) throw new RuntimeException(STD_ERROR_MSG);
        if(!locationService.connectionCallbacks.contains(callback)) {
            locationService.connectionCallbacks.add(callback);
            return true;
        }else return false;
    }

    public static boolean addFailedListener(GoogleApiClient.OnConnectionFailedListener listener){
        if(locationService==null) throw new RuntimeException(STD_ERROR_MSG);
        if(!locationService.failedListeners.contains(listener)) {
            locationService.failedListeners.add(listener);
            return true;
        }else return false;
    }

    public static boolean addOnClickListener(GoogleMap.OnMapClickListener listener){
        if(locationService==null) throw new RuntimeException(STD_ERROR_MSG);
        if(!locationService.onClickListeners.contains(listener)) {
            locationService.onClickListeners.add(listener);
            return true;
        }else return false;
    }


    private final Context context;
    private GoogleMap map;
    private final GoogleApiClient mGoogleApiClient;
    private boolean connected=false;
    private MarkerOptions selectedPoint;


    private ArrayList<GoogleApiClient.ConnectionCallbacks> connectionCallbacks = new ArrayList<>();
    private ArrayList<GoogleApiClient.OnConnectionFailedListener> failedListeners = new ArrayList<>();
    private ArrayList<GoogleMap.OnMapClickListener> onClickListeners = new ArrayList<>();

    private LocationService(GoogleMap map, Context context) {
        this.context = context;
        setMap(map);
        MapsInitializer.initialize(context);

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();

        map.setOnMapClickListener(this);
    }

    public void connect(){
        mGoogleApiClient.connect();
    }

    public void disconect(){
        mGoogleApiClient.disconnect();
    }

    public GoogleMap getMap() {
        return map;
    }

    public void setMap(GoogleMap map) {
        this.map = map;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
    }

    public boolean hasConnected(){ return connected;}

    public GoogleApiClient getApiClient(){ return mGoogleApiClient;}

    public Location getUserLocation(){
        Location location= LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if(location==null) location = findLocation();

        return location;
    }

    private Location findLocation() {
        String location_context = Context.LOCATION_SERVICE;
        LocationManager locationManager = (LocationManager) context.getSystemService(location_context);
        List<String> providers = locationManager.getProviders(true);
        for (String provider : providers) {
            locationManager.requestLocationUpdates(provider, 1000, 0,
                    new LocationListener() {

                        public void onLocationChanged(Location location) {}

                        public void onProviderDisabled(String provider) {}

                        public void onProviderEnabled(String provider) {}

                        public void onStatusChanged(String provider, int status,
                                                    Bundle extras) {}
                    });
            Location location = locationManager.getLastKnownLocation(provider);
            if(location!=null) return location;
        }

        return null;
    }

    public boolean setCameraInYourPosition(){
        Location location= getUserLocation();
        if(location!=null) {
            updateCameraView(new LatLng(location.getLatitude(),location.getLongitude()), MapZoom.STANDARD);
            return true;
        }else
            return false;

    }

    public void updateCameraView(LatLng latLng, MapZoom mapZoom){
        updateCameraView(latLng, mapZoom.getValue());
    }

    public void updateCameraView(LatLng latLng,int zoom){
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        map.animateCamera(cameraUpdate);

    }

    @Override
    public void onConnected(Bundle bundle) {
        connected=true;
        for(GoogleApiClient.ConnectionCallbacks callback : connectionCallbacks) callback.onConnected(bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {
        connected=false;
        for(GoogleApiClient.ConnectionCallbacks callback : connectionCallbacks) callback.onConnectionSuspended(i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        for(GoogleApiClient.OnConnectionFailedListener listener : failedListeners) listener.onConnectionFailed(connectionResult);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        for(GoogleMap.OnMapClickListener listener : onClickListeners) listener.onMapClick(latLng);

        selectedPoint=new MarkerOptions().position(latLng);

        map.clear();
        map.addMarker(selectedPoint);
    }
}

